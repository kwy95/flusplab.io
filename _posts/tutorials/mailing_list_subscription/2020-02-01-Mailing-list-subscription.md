---
layout: post
categories: [others, feed-english]
title:  "Mailing List Subscription"
date:   2020-02-01
author: marcelosc
lang: en
excerpt_separator: <!--end-abstract-->
---

# Introduction

In this tutorial, we will learn how to subscribe to Linux related mailing lists
such as git, dash, kernel janitors, and several kernel subsystems. Two
approaches will be described: one using neomutt, other using Gmail web
interface. We will also cover how to configure email filters so you don't get
your mailbox messed up after subscribing to the mailing lists. 

<!--end-abstract-->

{% include add_ref.html id="vger.kernel.org"
    title="VGER.KERNEL.ORG"
    author="VGER.KERNEL.ORG"
    year="2019"
    url="http://vger.kernel.org/" %}

{% include add_ref.html id="MajordomoLJ"
    title="Majordomo"
    author="Piers Cawley"
    year="1995"
    url="https://www.linuxjournal.com/article/1067" %}

{% include add_ref.html id="MajordomoMLM"
    title="Majordomo - mailing list manager"
    author="LinuxLinks"
    year="2019"
    url="https://www.linuxlinks.com/majordomo/" %}


{% include add_ref.html id="MajordomoInfo"
    title="VGER.KERNEL.ORG Majordomo Info"
    author="VGER.KERNEL.ORG"
    year="2019"
    url="http://vger.kernel.org/majordomo-info.html" %}

## VGER.KERNEL.ORG

[VGER.KERNEL.ORG](http://vger.kernel.org/) is a domain whose mission is to
provide email list services to Linux kernel developers{% include cite.html
id="vger.kernel.org" %}. Mailing list subscription is only handled by email. By
the time this tutorial was being written, no web interface was available for
subscription.

Most of Linux kernel related mailing lists are listed in the
[vger-lists](http://vger.kernel.org/vger-lists.html) page. The subscription
process in each of those lists is managed by a mailing list manager called
Majordomo {% include cite.html id="MajordomoLJ" %}{% include cite.html
id="MajordomoMLM" %}. It only takes (correct) actions when triggered by emails
in a very specific format. Emails sent to Majordomo **must** be in
**TEXT/PLAIN** (must not contain any HTML tag), should have no multipart
sections, nor anything "fancy"{% include cite.html id="MajordomoInfo" %}. These
rules apply to any email sent to Majordomo@vger.kernel.org, be it for
subscription, unsubscription, or anything else.

Anyway, some helpful links can be found on the vger-lists page. Every mailing
list has a `subscribe` link which provides both vger majordomo's email address
and a preformatted body to request subscription to the desired list. There is an
`unsubscribe` link with analogous properties too. We will use the information
provided by these links to request actions to Majordomo.

{% include add_image.html
   src="subscribe_link.png"
   caption="Subscribe link properties"%}

More information about vger's Majordomo and the commands it accepts can be
obtained sending a
<a href="MAILTO:majordomo@vger.kernel.org?body=help">help</a>
email to it.

## Subscribe with NeoMutt

[NeoMutt](https://neomutt.org/) is a command-line mail reader that easy the way
patches (code contributions) are sent and received. In this particular case,
NeoMutt will also easy the mailing list subscription process.

If NeoMutt is your default email reader, you might just click on the
`subscribe` link that NeoMutt will open a new email writing pane with `To:
majordomo@vger.kernel.org` set as the destination. As you press enter to confirm
it, the subject field will come empty (press enter again). If a confirmation
message appears warning that you are sending an email with no subject, press `n`
to tell NeoMutt you don't want to cancel the message. You will see the email
body loaded from the `subscribe` link. Save the message within your editor (if
you're using vim, type `:w` and press enter). Exit from your editor. You will
see a summary of the email you're about to send. It will show the following
fields:

* From: \<your email address\>
* To: majordomo@vger.kernel.org
* CC:
* Bcc:
* Subject:
* Reply-To:
* Security: None

Review the email fields and press `y` to send it to Majordomo. If another
confirmation message appears, press `n` to don't cancel the sending.

After some time Majordomo will send you an email with subject
"Confirmation for subscribe \<mailing list\>". Open the message to see the 
authentication code that lies inside. There will be a line that follows this
format:

	auth <code> subscribe <mailing list> <your email address>

Press `r` to answer Majordomo. Confirm Majordomo address. You may accept the
subject field or even erase it (Majordomo should not care about what the subject
field is). Type `n` to don't cancel if you have cleared the subject. Type `s` or
just press enter to include Majordomo's message in the answer. Erase everything
from the message body but the auth line. Also, erase the angular bracket and
space (`> `) before the auth.  Save the message, exit your editor, review the
fields, and type `y` to send your confirmation. If another confirmation message
appears, press `n` to tell NeoMutt you don't want to cancel the dispatch.

If NeoMutt is not your default email reader, you can go through almost the same
subscription steps. The most significant difference is that you might have to
write the body for the subscription request email. This is the string you see
after `body=` when you hover the mouse above the `subscribe` link. This string
should be `subscribe <list name>`, where \<list name\> is the mailing list
name you can see after `List: ` in the
[vger-lists](http://vger.kernel.org/vger-lists.html) page. Note that the email
body should contain **only** the subscribe string.
{: .info}

That's it. Within some time you shall receive a "Majordomo results" email
containing `Succeeded` and/or and welcoming email with the name of the mailing
list you requested subscription.

## E-mail filters within Gmail

To better organize the mailbox and avoid being somewhat flooded by the Linux
mailing lists (believe, a reasonable amount of emails may flow through a mailing
list during a day), it is of use to have one (or more) filter. The filter job is
to apply a label to mailing list emails and file them to a separate mailbox.
This way you will be able to see all (and only) the emails related to a Linux
subject within a defined box.

To do so, go to the settings page in the Gmail web interface.

{% include add_image.html
   src="email-settings.png"
   caption="Email settings option"%}

Click the "Filters and Blocked Addresses" tab and scroll down until you find a
"create a new filter" link. Then, click on it.

{% include add_image.html
   src="email-create-filter.png"
   caption="Create filter link at the 'Filters and Blocked Addresses' tab"%}

In the filter configuration pane, type the email address of your Linux list in
the "To" field and your own email address between `"` in the "Doesn't have"
field.  This will select any email going to the Linux mailing list of interest,
except those that are related to you. In other words, email answering your
questions, RFCs, patches, that go to the list will not be filed, instead, they
will remain in your default inbox so you can see them immediately when accessing
your email.

{% include add_image.html
   src="create_filter1.png"
   caption="Filter configuration pane"%}

Click the "Create filter" button. Check "Skip the Inbox (Archive it)", "Apply
the label", "Also apply filter to matching messages" and, if you feel like,
"Never send it to Spam". There’s a combo box beside the "Apply the label" to
allow you to select which label to apply. It is your personal choice the name
you want to give it. In this example, I used "Linux/iio" which is actually an
"iio" label nested under a "Linux" label. Finally, click the "Create filter"
button.

{% include add_image.html
   src="create_filter2.png"
   caption="Filter actions pane"%}

Some emails may be addressed to several mailing lists. For instance, a
devicetree binding doc patch may go addressed to both iio and devicetree mailing
lists. A security patch may go to crypto and janitors' lists. In such cases, how
can one know in which label an email will be accessible? The good news about
that is: it will be available in both. The labels are cumulative therefore, in
the above example, the email will be at both iio and devicetree boxes or, in
both crypto and janitors boxes. Indeed, if an email goes to more than one of the
mailing list you have subscribed, you will be able to see it in any of the label
related to it (provided that you have properly set the filters).

This shall prevent the mailing lists from flooding your inbox as well as keeping
things organized at your email account.

P.S.: Some of the prints are showing a "Continue" or "Update filter" button
instead of "Create filter" as I already had the filter I was using as example.
{: .info}

## Filters for FLUSP mailing lists (Bonus)

{% include add_image.html
   src="flusp_filter_pt1.png"
   caption="Filter configuration pane for FLUSP's kernel mailing list"%}

You don't need to check the "Skip the Inbox" option since you probably want to
see everything that is going on in the FLUSP contribuions list. =)

{% include add_image.html
   src="flusp_filter_pt2.png"
   caption="Filter actions pane for FLUSP's kernel mailing list"%}

## Unsubscribing

The unsubscribing process is analogous to the subscribe one. Clicking at one of
the `unsubscribe` links at [vger-lists](http://vger.kernel.org/vger-lists.html)
will call your default email reader tool setting destination to
majordomo@vger.kernel.org and body to `unsubscribe <mailing list>`. Once you
send the email to unsubscribe the list, Majordomo will take care of
unsubscribing your address from it.

## History

1. V1: Release

{% include print_bib.html %}

